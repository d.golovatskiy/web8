const getItemsFromLocalStorage = () => {
    if (localStorage.getItem('name') !== null)
      $('#name').val(localStorage.getItem('name'));
    if (localStorage.getItem('email') !== null)
      $('#email').val(localStorage.getItem('email'));
    if (localStorage.getItem('message') !== null)
      $('#mesasge').val(localStorage.getItem('message'));
    if (localStorage.getItem('checkbox') !== null) {
      $('#check').prop('checked', localStorage.getItem('checkbox') === 'true');
      if ($('#checkbox').prop('checked')) $('#send_button').removeAttr('disabled');
    }
  };
  
  const setItemsToLocalStorage = () => {
    localStorage.setItem('name', $('#name').val());
    localStorage.setItem('email', $('#email').val());
    localStorage.setItem('message', $('#message').val());
    localStorage.setItem('checkbox', $('#checkbox').prop('checked'));
  };
  
  const clearLocalStorage = () => {
    localStorage.clear();
    $('#name').val('');
    $('#email').val('');
    $('#message').val('');
    $('#checkbox').val(false);
  };
  $(document).ready(function () {
    getItemsFromLocalStorage();
    $('.open_button').click(() => {
      $('.Hdiv').css('display', 'flex');
      history.pushState(true, '', './form');
    });
    $('.close_button').click(function () {
      $('.Hdiv').css('display', 'none');
      history.pushState(false, '', '.');
    });
    $('#form').submit(function (e) {
      e.preventDefault();
      $('.Hdiv').css('display', 'none');
      let data = $(this).serialize();
      let name;
      if ($('#name').val() !== '') name = $('#name').val();
      else name = 'user';
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'https://formcarry.com/s/omsB_c8SzM',
        data: data,
        success: function () {
          alert("Thanks for visit " + name);
          clearLocalStorage();
        },
      });
    });
    $('#checkbox').change(function () {
      if (this.checked) $('#send_button').removeAttr('disabled');
      else $('#send_button').attr('disabled', '');
    });
    $('#form').change(setItemsToLocalStorage);
  
    window.onpopstate = function (e) {
      if (e.state) $('.Hdiv').css('display', 'flex');
      else $('.Hdiv').css('display', 'none');
    };
  });
